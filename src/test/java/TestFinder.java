import com.relayr.Finder;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by erdem on 25.07.2017.
 * TestFinder test object
 * Tests every single method in the Finder class
 * Naming Conventions re used as " MethodName_ExpectedBehavior_StateUnderTest "
 */
public class TestFinder {

    // Mock data is defined as source for string manipulations without assigning any value
    String[] mockArray;
    // CHARACTER_DICT is defined to use as character pool for generating randomized strings.
    private final String CHARACTER_DICT = "abcdefghijklmnopqrstuvwxyz";


    /** Helper method generates randomized string
     **/
    private String getRandomString(int count) {
        StringBuilder builder = new StringBuilder();
        while (count-- != 0) {
            int character = (int)(Math.random()*CHARACTER_DICT.length());
            builder.append(CHARACTER_DICT.charAt(character));
        }
        return builder.toString();
    }

    /** Tests find method in the Finder Class if a search key is not initialized
     *  Expected behaviour is NullPointerException
     **/
    @Test(expected = NullPointerException.class)
    public void testFind_NullPointerException_IfSearchKeyNull() {
        Finder finder = new Finder(mockArray);
        finder.find(null);
    }

    /** Tests find method in the Finder Class if the source array is not initialized
     *  Expected behaviour is NullPointerException
     **/
    @Test(expected = NullPointerException.class)
    public void testFind_NullPointerException_IfSourceArrayNull() {
        Finder finder = new Finder(mockArray);
    }


    /** Tests isSubSequence method in the Finder Class if at least one method parameter is not initialized
     *  Expected behaviour is NullPointerException
     **/
    @Test(expected = NullPointerException.class)
    public void testIsSubSequence__NullPointerException_AreParamsNull() {
        Finder finder = new Finder(mockArray);
        finder.find(null);
    }


    /** Tests find method in the Finder Class when the source is around ~100 mb
     **/
    @Test
    public void testFind_ExpectedEqual_TwoHugeArraysAreSame() {
        mockArray = new String[10000000]; // Mock source is initialized
        for (int i = 0 ; i <10000000 ; i++ ){
            mockArray[i] = this.getRandomString(10) ; // every single element in mock source is assigned to randomized string
        }
        Finder finder = new Finder(mockArray); // Finder is initialized with source array
        String[] results = finder.find("ae"); // searching "ae" keyword in source array and returns result string array

        Finder newFinder = new Finder(results); // Finder is initialized with the new source created above
        assertArrayEquals(results,newFinder.find("ae")); // Checking if two array is equal or not. Here, resulted must be same as the source above

    }

    /** Tests find method in the Finder Class when all elements in the source are same and contains a specified string
     **/
    @Test
    public void testFind_ExpectedEqual_IfAllElementsAreSame() {
        mockArray = new String[10000];
        for (int i = 0 ; i <10000 ; i++ ){
            mockArray[i] = "test" ;
        }
        Finder finder = new Finder(mockArray);
        assertEquals(10000,finder.find("et").length);

    }

    /** Tests find method in the Finder Class when all elements in the source are same and don't contain a specified string
     **/
    @Test
    public void testFind_ExpectedEqual_IfAllElementsAreNotSame() {
        mockArray = new String[10000];
        for (int i = 0 ; i <10000 ; i++ ){
            mockArray[i] = "test" ;
        }
        Finder finder = new Finder(mockArray);
        assertEquals(0,finder.find("o").length);

    }

    /** Tests isSubSequence method in the Finder Class when a source string contains a string
     **/
    @Test
    public void testIsSubSequence_ExpectedEqual_IfSourceContains() {
        mockArray = new String[10];
            for (int i = 0 ; i <10 ; i++ ){
            mockArray[i] = this.getRandomString(5) ;
        }
        Finder finder = new Finder(mockArray);
        assertTrue(finder.isSubSequence("dos","sidious"));
    }

    /** Tests isSubSequence method in the Finder Class when a source string does not contain a string
     **/
    @Test
    public void testIsSubSequence_ExpectedEqual_IfSourceDoesNotContains() {
        mockArray = new String[10];
        for (int i = 0 ; i <10 ; i++ ){
            mockArray[i] = this.getRandomString(5) ;
        }
        Finder finder = new Finder(mockArray);
        assertTrue(!finder.isSubSequence("123","sidious"));
    }

}
