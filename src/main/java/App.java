import com.relayr.Finder;

/**
 * Created by erdem on 25.07.2017.
 * App class that has an entry point for the applciation
 */
public class App {

    public static void main(String[] args) {
        String[] sourceArray = {"test1" , "test2" , "test3" , "test4", "test5", "test6","test7"};
        Finder finderInstance = new Finder(sourceArray);
        String[] resultArray = finderInstance.find("s4");
        for (int i = 0; i < resultArray.length; i++) {
            System.out.println(resultArray[i]); // gives only "test4" output
        }
    }
}
