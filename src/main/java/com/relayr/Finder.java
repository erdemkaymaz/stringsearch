package com.relayr;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by erdem on 25.07.2017.
 * Finder Model Object
 * Note that Finder class provides string manipulations
 */
public class Finder {
    private String[] sourceKeyArray ;


    /**
     * Constructor.
     * @param sourceArray (not nullable) is an essential source of string manipulations
     */
    public Finder(String[] sourceArray) {
        if(sourceArray == null){
            throw new NullPointerException();
        }
        this.sourceKeyArray = sourceArray;
    }

    /** Searches a key in the source string array
     *  @param searchKey (not nullable) the string to be processed in a source string array
     *  Complexity O(n * m)
     * */
    public String[] find(String searchKey){

        if(searchKey == null){
            throw new NullPointerException(); // Throws NullPointerException if search key is null
        }

        List<String> resultList = new ArrayList<String>();
        for (int i = 0; i < sourceKeyArray.length; i++) {
            if(isSubSequence(searchKey,sourceKeyArray[i])){
                resultList.add(sourceKeyArray[i]);
            }
        }
        return resultList.toArray(new String[resultList.size()]);
    }

    /** Checks whether @searchkey is in @sourcekey or not.
     * @param searchKey (not nullable) the string to be processed in a source string
     * @param sourceKey (not nullable) the string to be used as a source string
     * Complexity O(n)
     * */
    public boolean isSubSequence(String searchKey, String sourceKey)
    {
        // Null check for parameters
        if(searchKey == null ||sourceKey == null){
            throw new NullPointerException(); // throws NullPointerException when at least one paramter is null
        }

        int seachKeyLength = searchKey.length();
        int sourceKeyLength = sourceKey.length();
        int j = 0;

        // Traverse sourceKey and searchKey, and compare current character
        // of sourceKey with first unmatched char of searchKey, if matched
        // then move ahead in searchKey
        for (int i=0; i<sourceKeyLength&&j<seachKeyLength; i++)
            if (searchKey.charAt(j) == sourceKey.charAt(i))
                j++;

        // If all characters of searchKey were found in sourceKey
        return (j==seachKeyLength);
    }
}
